#ifndef CANVAS_H
#define CANVAS_H

#include <Arduino.h>
#include <RGBA.h>

class Canvas {

public:

	virtual size_t getWidth() = 0;
	virtual size_t getHeight() = 0;
	
	virtual void setPixelColor(size_t x, size_t y, RGBA color) = 0;
	virtual void commit() = 0;

};


#endif //CANVAS_H
